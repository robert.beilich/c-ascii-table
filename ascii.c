#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const int MAX_CHAR = 255;
const int MIN_CHAR = 32;
const int CHAR_COUNT = MAX_CHAR-MIN_CHAR+1;

void line(int column_count) {
  for(int i=MIN_CHAR; i <= MAX_CHAR; i++) {
    printf("|%3d\t%X\t%c\t", i, i, i);
    if ((i-MIN_CHAR) % column_count == column_count - 1) {
      printf("|\n");
    }
  }
  if (CHAR_COUNT % column_count != 0) {
    printf("|\n");
  }
}

void column(int column_count) {
  int lines = (CHAR_COUNT)/column_count;
  if (CHAR_COUNT % column_count > 0) {
    lines++;
  }
  for(int i=0; i<lines; i++) {
    for(int j=0; j < column_count && MIN_CHAR+i+j*lines <= MAX_CHAR; j++) {
      int ch = MIN_CHAR+i+j*lines;
      printf("|%3d\t%X\t%c\t", ch, ch, ch);
    }
    printf("|\n");
  }
}

void error() {
  printf("Usage: ascii <line|column> <column count>");
  exit(1);
}

void header(int column_count) {
  for (int i=0; i<column_count; i++) {
    printf("|dec\thex\tchar\t");
  }
  printf("|\n");
}

int main(int argc, char* argv[]) {
  if (argc != 3) {
    error();
  }
  int columns = strtol(argv[2], NULL, 10);
  if(strcmp(argv[1], "line") == 0) {
    header(columns);
    line(columns);
  } else {
    if(strcmp(argv[1], "column") == 0) {
      header(columns);
      column(columns);
    } else {
      error();
    }
  }
}
